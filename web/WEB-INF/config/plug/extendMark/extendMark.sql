
-- 创建函数

CREATE FUNCTION getChildChannelList (rootId INT) RETURNS VARCHAR (1000)
BEGIN
	DECLARE
		sChildList VARCHAR (1000);
  DECLARE
	  sChildTemp VARCHAR (1000);
SET sChildTemp = cast(rootId AS CHAR);
WHILE sChildTemp IS NOT NULL DO
IF (sChildList IS NOT NULL) THEN
SET sChildList = concat(sChildList, ',', sChildTemp);
ELSE
SET sChildList = concat(sChildTemp);
END
IF;
SELECT
	group_concat(channel_id) INTO sChildTemp
FROM
	jc_channel
WHERE
	FIND_IN_SET(parent_id, sChildTemp) > 0;
END
WHILE;
RETURN sChildList;
END;