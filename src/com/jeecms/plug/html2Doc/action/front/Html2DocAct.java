package com.jeecms.plug.html2Doc.action.front;

import com.jeecms.cms.entity.main.ContentDoc;
import com.jeecms.common.web.ResponseUtils;
import com.jeecms.common.web.springmvc.RealPathResolver;
import com.jeecms.core.entity.CmsSite;
import com.jeecms.core.web.WebErrors;
import com.jeecms.core.web.util.CmsUtils;
import com.jeecms.plug.html2Doc.util.Html2DocUtil;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * html 转换为 world
 * author dingshuang
 * date 2016-06-06
 */
@Controller
public class Html2DocAct {

    private static final Logger log = LoggerFactory.getLogger(Html2DocAct.class);

    @Autowired
    private RealPathResolver realPathResolver;

    /**
     * 文件生成路径
     */
    public static final String CREAT_PATH = "/c/cms/";

    /**
     * 通过url将网页转换为world
     *
     * @param request
     * @param response
     * @param model
     * @return
     */
    @RequestMapping("/html_2_doc.jspx")
    public void input(HttpServletRequest request, HttpServletResponse response, ModelMap model) {
        CmsSite site = CmsUtils.getSite(request);
        String url = request.getParameter("url");
        String nodeId = request.getParameter("nodeId");
        String addStyle = request.getParameter("addStyle");
        String fileName = request.getParameter("fileName");
        String clearIds[] = null;
        if (StringUtils.isNotBlank(request.getParameter("clearIds"))) {
            clearIds = request.getParameter("clearIds").split(",");
        }
        //生成文件
        String content = Html2DocUtil.getContent(url, nodeId, clearIds, addStyle);
        String filePath = Html2DocUtil.createWordFile(fileName, content, this.getFilePath(site));
        //下载文件
        try {
            this.downLoadFile(request, response, filePath, fileName);
            //删除生成的文件，避免过分累积而消耗服务器空间
            this.deletFile(filePath);
        } catch (IOException e) {
            log.info("文件下载出现异常！");
        }

    }


    /**
     * 该方法用于下载文件
     *
     * @param request
     * @param response
     * @param filePath
     * @param fileName
     * @throws IOException
     */
    private void downLoadFile(HttpServletRequest request,
                              HttpServletResponse response, String filePath, String fileName) throws IOException {
        CmsSite site = CmsUtils.getSite(request);
        // 解决中文乱码
        filePath = encodeFilename(request, filePath);
        File file = new File(filePath);
        if (file.exists()) {
            byte[] byteArray = FileUtils.readFileToByteArray(file);
            response.addHeader("Content-Disposition" , "attachment;filename=" + fileName + ".doc");
            response.setContentType("application/x-msdownload");
            ServletOutputStream out = response.getOutputStream();
            out.write(byteArray);
            out.close();
        } else {
            ResponseUtils.renderJson(response, WebErrors.create(request).getMessage("文件不存在或被删除！"));
        }
    }


    private String encodeFilename(HttpServletRequest request, String fileName) {
        String agent = request.getHeader("USER-AGENT");
        try {
            // IE
            if (null != agent && -1 != agent.indexOf("MSIE")) {
                fileName = URLEncoder.encode(fileName, "UTF8");
            } else {
                fileName = new String(fileName.getBytes("utf-8"), "iso-8859-1");
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return fileName;
    }


    /**
     * 获取文件的生成路径
     *
     * @param site
     * @return
     */
    private String getFilePath(CmsSite site) {
        String path = "";
        String ctx = site.getContextPath();
        if (StringUtils.isNotBlank(ctx)) {
            path = CREAT_PATH.substring(ctx.length());
        }
        // 文件绝对路径
        String fileRealPath = realPathResolver.get(path) + site.getPath();
        return fileRealPath;
    }

    /**
     * 该方法用于删除文件
     *
     * @param filePath
     * @return
     */
    private boolean deletFile(String filePath) {
        File file = new File(filePath);
        return file.delete();
    }

}
